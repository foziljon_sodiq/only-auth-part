"use client";
import { signIn, signOut, useSession } from "next-auth/react";
import Link from "next/link";
const Nav = () => {
  const { data: session, status } = useSession();
  console.log(session, status);
  return (
    <div className="flex justify-between py-6 px-10 bg-sky-200">
      <h1>
        <Link href={"/"}>BRAND</Link>
      </h1>

      <div className="flex gap-4 items-center">
        <Link href="/about">About</Link>
        <Link href="/contact">Contact</Link>
        {session?.user && <Link href="/dashboard">Dashboard</Link>}
      </div>

      {status == "loading" && <p>loading...</p>}
      {status == "unauthenticated" && (
        <button
          className="btn rounded-full bg-sky-500 px-2 py-1 text-white"
          onClick={() => signIn()}
        >
          Sign In
        </button>
      )}
      {status == "authenticated" && (
        <div className="flex gap-4">
          <button
            className="btn rounded-full bg-sky-500 px-2 py-1 text-white"
            onClick={() => signOut()}
          >
            Sign Out
          </button>

          <div className="flex items-center gap-4">
            <p>{session?.user?.name}</p>
            <img
              className="w-8 h-8 rounded-full"
              src={session?.user?.image}
              alt="user photo"
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default Nav;
